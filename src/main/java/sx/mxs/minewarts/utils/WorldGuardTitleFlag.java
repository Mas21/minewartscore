package sx.mxs.minewarts.utils;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.session.MoveType;
import com.sk89q.worldguard.session.Session;
import com.sk89q.worldguard.session.handler.FlagValueChangeHandler;
import com.sk89q.worldguard.session.handler.Handler;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import java.util.Collections;
import java.util.Set;


public abstract class WorldGuardTitleFlag extends FlagValueChangeHandler<Boolean> {

    public final static BooleanFlag GREETING_TITLE = new BooleanFlag("greeting-title");

    public static final Factory FACTORY = new Factory();
    public static class Factory extends Handler.Factory<WorldGuardTitleFlag> {
        @Override
        public WorldGuardTitleFlag create(Session session) {
            return new WorldGuardTitleFlag(session) {
                protected void onInitialValue(Player player, ApplicableRegionSet applicableRegionSet, Boolean aBoolean) {

                }

                protected boolean onSetValue(Player player, Location location, Location location1, ApplicableRegionSet applicableRegionSet, Boolean aBoolean, Boolean t1, MoveType moveType) {
                    return false;
                }

                protected boolean onAbsentValue(Player player, Location location, Location location1, ApplicableRegionSet applicableRegionSet, Boolean aBoolean, MoveType moveType) {
                    return false;
                }
            };
        }
    }

    private Set<String> lastMessageStack = Collections.emptySet();

    public WorldGuardTitleFlag(Session session) {
        super(session, GREETING_TITLE);
    }

    @Override
    public boolean onCrossBoundary(Player player, Location from, Location to, ApplicableRegionSet toSet, Set<ProtectedRegion> entered, Set<ProtectedRegion> exited, MoveType moveType) {
        player.sendMessage("Test.");
        return true;
    }

    protected void onInitialValue(Player player, ApplicableRegionSet applicableRegionSet, StateFlag.State state) {

    }

    protected boolean onSetValue(Player player, Location location, Location location1, ApplicableRegionSet applicableRegionSet, StateFlag.State state, StateFlag.State t1, MoveType moveType) {
        return false;
    }

    protected boolean onAbsentValue(Player player, Location location, Location location1, ApplicableRegionSet applicableRegionSet, StateFlag.State state, MoveType moveType) {
        return false;
    }


}