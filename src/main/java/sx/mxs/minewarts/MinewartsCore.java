package sx.mxs.minewarts;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import sx.mxs.minewarts.utils.WorldGuardTitleFlag;

import java.io.File;

public class MinewartsCore extends JavaPlugin {

    public static WorldGuardPlugin worldGuardPlugin;

    Plugin plugin;



    public String title, subtitle, titleregionid;
    public int fadeintime, staytime, fadeouttime;


    @Override
    public void onLoad() {
        MinewartsCore.worldGuardPlugin = (WorldGuardPlugin) this.getServer().getPluginManager().getPlugin("WorldGuard");
        MinewartsCore.worldGuardPlugin.getFlagRegistry().register(WorldGuardTitleFlag.GREETING_TITLE);
    }

    @Override
    public void onEnable() {
        final FileConfiguration config = this.getConfig();
        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            config.options().copyDefaults(true);
        }
        this.saveConfig();
        this.setupHandlers();
        this.setupListeners();
        this.setupCommands();
        this.setupWorldGuardFlags();
    }


    public void setupListeners() {
    }

    public void setupHandlers() {
        PluginManager pm = getServer().getPluginManager();
    }

    public void setupCommands() {
    }

    public void setupWorldGuardFlags() {
        title = plugin.getConfig().getString("Region-Title.title");
        subtitle = plugin.getConfig().getString("Region-Title.subtitle");
        fadeintime = plugin.getConfig().getInt("Region-Title.fade-in-time");
        staytime = plugin.getConfig().getInt("Region-Title.stay-time");
        fadeouttime = plugin.getConfig().getInt("Region-Title.fade-out-time");
        titleregionid = plugin.getConfig().getString("Region-Title.region-id");
    }




}
